package web;

import models.Ticket;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.AbstractPage;
import pages.CreateTicketPage;
import pages.LoginPage;
import pages.TicketsPage;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class HelpdeskUITest {

    private WebDriver driver;

    @BeforeClass
    public void setup() throws IOException {

        // Читаем конфигурационные файлы в System.properties
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("config.properties"));
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("user.properties"));
        // Создание экземпляра драйвера
        System.setProperty("webdriver.chrome.driver","C:\\myprog\\Googl\\chromedriver.exe");
        driver = new ChromeDriver();
        // Устанавливаем размер окна браузера, как максимально возможный
        driver.manage().window().maximize();
        // Установим время ожидания для поиска элементов
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // Установить созданный драйвер для поиска в веб-страницах
        AbstractPage.setDriver(driver);
    }

    @Test
    public void createTicketTest() throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        // todo: открыть главную страницу
        driver.get("https://at-sandbox.workbench.lanit.ru");
        TimeUnit.SECONDS.sleep(5);
         // Заполняем объект класс Ticket необходимыми тестовыми данными
        Ticket ticket = buildNewTicket();
        // todo: создать объект главной страницы и выполнить шаги по созданию тикета
        Select queue = new Select(driver.findElement(By.xpath("//select[@id='id_queue']")));
        queue.selectByVisibleText(ticket.getQueue());
        Select priority = new Select(driver.findElement(By.xpath("//select[@id='id_priority']")));
        priority.selectByVisibleText(ticket.getPriority());
        CreateTicketPage createTicketPage = PageFactory.initElements(driver, CreateTicketPage.class);
        createTicketPage.createTicket(ticket);
        // todo: перейти к странице авторизации и выполнить вход
        driver.findElement(By.xpath("//a[@href='/login/?next=/']")).click();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.login("admin", "adminat");
        driver.findElement(By.id("search_query")).sendKeys(ticket.getAddress());
        driver.findElement(By.xpath("//div[@class='input-group-append']")).click();
        TicketsPage ticketsPage = PageFactory.initElements(driver, TicketsPage.class);
        try {
            ticketsPage.openTicket(ticket);} catch (Exception e){
            System.out.println("Problem");
        }
        System.out.println(ticket.equalsTicket());
        // todo: найти созданный тикет и проверить поля

        // Закрываем текущее окно браузера
        driver.close();
    }

    /**
     * Создаём и заполняем объект тикета
     *
     * @return заполненный объект тикета
     */

    protected Ticket buildNewTicket(){
        Ticket ticket = new Ticket();

        ticket.setQueue("Some Product");
        ticket.setPriority("2. High");
        ticket.setTitle("Test this problem");
        ticket.setDescription("I want to sleep");
        ticket.setAddress("papam@mail.ru");

        // todo: заполнить остальные необходимые поля тикета

        return ticket;
    }

    @AfterTest
    public void close() {
        // Закрываем все окна браузера и освобождаем ресурсы
        driver.quit();
    }

}
