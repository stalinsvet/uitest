package models;

import pages.TicketPage;

/** Объект тикета */
public class Ticket {
    TicketPage openTicket = new TicketPage();
    /** Название проблемы */
    private String title;
    private String description;
    private String address;
    private String priority;
    private String queue;

    public Ticket() {
    }
    // todo: остальные поля необходимые для заполнения тикета

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // todo: методы get и set для остальных полей

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }
    // todo: equals и hashCode

    public boolean equalsTicket(){
        openTicket.getTicketTitle().equals(getTitle());
        openTicket.getEmail().equals(getAddress());
        openTicket.getQueue().equals(getQueue());
        openTicket.getDescription().equals(getDescription());
        openTicket.getPriority().equals(getPriority());
        return equalsTicket();
    }
}
