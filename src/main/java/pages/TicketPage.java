package pages;

import models.Ticket;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Страница отдельного тикета
 */
public class TicketPage extends HelpdeskBasePage {
    Ticket ticket = new Ticket();
    @FindBy(xpath = "//h3")
    private WebElement ticketTitle;
    // todo: остальные поля тикета
    @FindBy(xpath = "//th[text()='Submitter E-Mail']/following-sibling::td[1]")
    private WebElement email;
    @FindBy(xpath = "//td[@class='table-warning']")
    private WebElement prioroty;
    @FindBy(xpath = "//thead/tr/th/text()")
    private WebElement queue;
    @FindBy(xpath = "//td[@id=\"ticket-description\"]/p")
    private WebElement description;

    public TicketPage() {
        PageFactory.initElements(driver, this);
    }

    /**
     * Получить имя тикета
     */
    public String getTicketTitle() {
        return ticketTitle.getText().substring(9, (9 + ticket.getTitle().length()));
    }

    public String getPriority(){
        return prioroty.getText();
    }
    /**
     * Получить адрес почты
     */
    public String getEmail() {
        // Получаем значение адреса почты
        return email.getText();
    }

    public String getQueue(){
        return queue.getText().substring(9);
    }
    public String getDescription(){
        return description.getText();
    }
    // todo: остальные методы получения значений полей

    /**
     * Получить значение элемента таблицы
     *
     * @param columnElem элемент ячейки для которой нужно вернуть значение
     * @return текстовое значение ячейки рядом
     */
    private String getValue(WebElement columnElem) {
        return columnElem
                // Находи следующий элемент находящийся в том же теге
                .findElement(By.xpath(""))
                // Получаем текст
                .getText()
                // Обрезаем лишние пробелы
                .trim();
    }

}
