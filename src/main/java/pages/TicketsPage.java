package pages;

import models.Ticket;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Страница со списком тикетов
 */
public class TicketsPage extends HelpdeskBasePage {

    List<WebElement> webElementTicket = driver.findElements(By.xpath("//tbody/tr[@role='row']"));

    // todo: элементы страницы поиска тикетов

    public TicketsPage() {
        PageFactory.initElements(driver, this);
    }

    /**
     * Ищем строку с тикетом и нажимаем на нее
     */
    public void openTicket(Ticket ticket) {
        for (int i = 0; i < webElementTicket.size(); i++) {
            String savedTicket = webElementTicket.get(i).findElement(By.xpath("//tbody/tr[@role='row']//div/a")).getText().substring(6);
            System.out.println(savedTicket);
            System.out.println(ticket.getTitle().equals(savedTicket));
            if (ticket.getTitle().equals(savedTicket)) {
                webElementTicket.get(i).findElement(By.xpath("//tbody/tr[@role='row']//div/a")).click();
            }
        }
    }
}
