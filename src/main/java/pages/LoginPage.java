package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Страница авторизации
 */
public class LoginPage extends HelpdeskBasePage {

    // todo: элементы страницы
    @FindBy(id = "username")
    private WebElement username;
    @FindBy(id = "password")
    private WebElement pass;
    @FindBy(xpath = "//input[@class='btn btn-lg btn-primary btn-block']")
    private WebElement login;

    public LoginPage() {
        PageFactory.initElements(driver, this);
    }



    /**
     * Авторизация пользователя
     *
     * @param user     логин пользователя
     * @param password пароль пользователя
     */
    public void login(String user, String password) {
        // todo: заполнить поля и нажать кнопку авторизации
        typeUsername(user);
        typePass(password);
        clickLogin();
    }

// todo: методы работы с элементами

    public LoginPage typeUsername(String user) {
        username.sendKeys(user);
        return this;
    }

    public LoginPage typePass(String password) {
        pass.sendKeys(password);
        return this;
    }

    public LoginPage clickLogin() {
        login.click();
        return new LoginPage();
    }


}
