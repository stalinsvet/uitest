package pages;

import models.Ticket;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

/**
 * Страница создания тикета
 */
public class CreateTicketPage extends HelpdeskBasePage {

    public CreateTicketPage() {
        // Необходимо инициализировать элементы класса, лучше всего это делать в конструкторе
        PageFactory.initElements(driver, this);
    }

    // Способ объявления элементы страницы, через аннотацию
    @FindBy(xpath = "//div/input[@name='title']")
    private WebElement inputProblemTitle;
    // todo: остальные элементы
    @FindBy(xpath = "//div/textarea")
    private WebElement description;
    @FindBy(xpath = "//input[@name='submitter_email']")
    private WebElement address;
    @FindBy(xpath = "//button[@class='btn btn-primary btn-lg btn-block']")
    private WebElement create;



    /**
     * Создание тикета
     */
    public void createTicket(Ticket ticket) throws InterruptedException {
        setProblemTitle(ticket.getTitle());
        setDescription(ticket.getDescription());
        setAddress(ticket.getAddress());
        // todo: заполнение остальных полей
        createTicket();
    }


    /**
     * Заполнение поля "Summary of the problem"
     */
    public CreateTicketPage setProblemTitle(String text) {
        inputProblemTitle.sendKeys(text);
        return this;
        // todo: заполнить поле
    }

    // todo: методы заполнения остальных полей
    public CreateTicketPage setDescription(String text) {
        description.sendKeys(text);
        return this;
    }

    public CreateTicketPage setAddress(String text) {
        address.sendKeys(text);
        return this;
    }

    /**
     * Зажатие кнопки "Submit Ticket"
     */
    public CreateTicketPage createTicket() {
        create.click();
        return new CreateTicketPage();
        // todo: нажать кнопку создания задания
    }

}
